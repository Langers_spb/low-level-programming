global _start
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global read_char
global read_word
global string_equals
global string_copy
global parse_int
global parse_uint

section .data
message: db 'qwertyqwertyqwerty', 0
newline: db 10

section .text

exit:
	mov rax, 60
	syscall
	ret

string_length:
	xor rax, rax
.loop:
	cmp byte[rdi+rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	ret

print_string:
	mov rsi, rdi
	call string_length
	mov rdx, rax
	mov rax, 1
	mov rdi, 1
	syscall
	ret

print_char:
	push rdi
	mov rax, 1
	mov rdi, 1
	mov rdx, 1
	mov rsi, rsp
	syscall
	pop rdi
	ret

print_newline:
	xor rax, rax
	mov rdi, newline
	call print_char
	ret

print_uint:
	mov rax, rdi
	mov r10, 10
	push 0x00
.loop:
	xor rdx, rdx
	div r10
	add rdx, '0'
	push rdx
	test rax, rax
	jnz .loop
.out:
	pop rdi
	cmp rdi, 0x00
	jz .exit
	call print_char
	jmp .out
.exit:
	ret

print_int:
	test rdi, rdi
	jns .positive
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
.positive:
	call print_uint
	ret	

read_char:
	push 0
	xor rax, rax
	xor rdi, rdi
	mov rdx, 1
	mov rsi, rsp
	syscall
	pop rax
	ret

read_word:
        mov r9, rdi
        mov r10, rsi
        push r9
        xor r8, r8
        mov r8, 0
.loop1:
        push r9
        push r10
        push r8
        call read_char
        pop r8
        pop r10
        pop r9
        test rax, rax
        jz .end
        cmp rax, ' '
        jle .loop1
.loop2:
        cmp  r8, r10
        jae .not
        mov [r9], al
        inc r9
        inc r8
        push r9
        push r10
        push r8
        call read_char
        pop r8
        pop r10
        pop r9
        cmp rax, ' '
        jg .loop2
.end:
        mov byte[r9], 0x00
        pop r9
        mov rax, r9
        mov rdx, r8
        ret
.not:
        pop r9
        mov byte[r9], 0x00
        mov rax, 0
        ret
	
parse_uint:
        xor rax, rax
        xor r9, r9
        xor r8, r8
        mov rcx, 10
        push r15
        mov r13, 0

.loop2:
        cmp byte[rdi+r9], 48
        jb .no
        cmp byte[rdi+r9], 57
        ja .no
        xor rdx, rdx
        mul rcx
        mov r8b, byte[rdi+r9]
        sub r8b, '0'
        add rax, r8
        inc r15
        inc r9
        jmp .loop2

.no:
        cmp r15, 0
        jne .end
        xor rax, rax
        xor rdx, rdx
        ret
.end:
        mov rdx, r15
        pop r15
        ret

parse_int:
	cmp byte[rdi], '-'
	jne parse_uint
	inc rdi
	call parse_uint
	inc rdx
	neg rax
	ret

string_equals:
    xor rax, rax
    call string_length
    mov rcx, rax
    push rdi
    mov rdi, rsi
    call string_length
    pop rdi
    cmp rcx, rax
    js .not
    cld
    repe cmpsb
    test rcx, rcx
    jnz .not  
.yes:
     mov rax, 0x01
     ret
.not:
     mov rax, 0x00
     ret

string_copy:
	xor rax, rax
.loop:
	xor rcx, rcx
	mov cl, byte[rdi]
	mov byte[rsi], cl
	cmp rsi, rdx                           
	jz .exit
	inc rdi
	inc rsi
	test rcx, rcx
	jnz .loop
.exit:
	xor rax, rax
	ret